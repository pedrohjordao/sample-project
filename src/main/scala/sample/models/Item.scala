package sample.models

import sample.models.Item._

/**
  * Aggregation of data that represents purchasable item
  *
  * @param shippingFee fee used to ship the item
  * @param tax         tax for the item
  * @param products    products contained in this item
  */
final case class Item(
    shippingFee: Fee,
    tax: Tax,
    products: Seq[Product]
) {
  val cost: Cost =
    (1.0 + tax) * products.map(_.price).foldRight(0.0)(_ + _) + shippingFee
}
object Item {
  type Cost = Double
  type Fee = Double
  type Tax = Double
}
