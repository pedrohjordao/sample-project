package sample.models

import sample.models.Customer._

/**
  * Data aggregation for a customer
  *
  * @param name    the name of the customer
  * @param contact contact information for tue customer
  */
final case class Customer(name: Name, contact: Contact)
object Customer {
  type Name = String
  type Contact = String
}
