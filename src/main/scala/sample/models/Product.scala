package sample.models

import java.time.LocalDateTime

import sample.models.Product._

/**
  * Data aggregation for a product
  *
  * @param name         name of the product
  * @param category     category of the product
  * @param weight       weight of the product
  * @param price        price of the product
  * @param creationDate creation date of the product
  */
final case class Product(
    name: String,
    category: ProductCategory,
    weight: Weight,
    price: Price,
    creationDate: LocalDateTime
)
object Product {
  type ProductCategory = String
  type Weight = Double
  type Price = Double
}
