package sample.models

import java.time.LocalDateTime

import sample.models.Order._

/**
  * Order information of a customer
  *
  * @param customerInfo       information about the customer who requested this order
  * @param shippingAddress    address to ship this order to
  * @param orderPlacementDate date this order was placed
  * @param items              items contained in this order
  */
final case class Order(
    customerInfo: Customer,
    shippingAddress: Address,
    orderPlacementDate: LocalDateTime,
    items: Seq[Item]
) {
  val totalCost = items.map(_.cost).foldRight(0.0)(_ + _)
}
object Order {
  type Address = String
}
