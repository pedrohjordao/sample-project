package sample

import java.time.LocalDateTime

import scala.util.{Failure, Success, Try}

/**
  * A interval of months created through a String configuration
  *
  * ## Note
  *
  * Probably should migrate to using [[java.time.Period]]
  */
final case class Interval private (
    asString: String,
    private val comparator: (LocalDateTime, LocalDateTime) => Boolean
) {

  /**
    * Checks if this [[Interval]] in reference to a given start contains the `value` date
    *
    * @param startReference the reference to which the interval is applied
    * @param value          value to be checked if belongs to the interval
    * @return true if `value` belongs to interval, false otherwise
    */
  def contains(startReference: LocalDateTime, value: LocalDateTime): Boolean =
    comparator(startReference, value)
}
object Interval {

  private val intervalPattern = "([0-9]+)-([0-9]+)".r
  private val boundedPattern = "(>|<)([0-9]+)".r

  /**
    * From a given configuration String produces an interval
    *
    * The configuration String must be in the format
    *
    * {{{
    * M1-M2 where M1 <= M2, represents the interval between M1 and M2, including M1 and M2
    * >M represents months after M, including M
    * <M represents months before M, including M
    * }}}
    *
    * While this is a month based interval, the day, hour and minutes are also taken into consideration
    *
    * @param intervalString the configuration String
    * @return an [[Interval]] if the parsing was successful
    */
  def from(intervalString: String): Try[Interval] = {
    val comparatorFunction = intervalString match {
      case intervalPattern(initialMonth, finalMonth)
          if initialMonth.toInt > 0 && initialMonth.toInt <= finalMonth.toInt => {
        val initMonth = initialMonth.toInt
        val endMonth = finalMonth.toInt

        Success((start: LocalDateTime, value: LocalDateTime) => {
          val lowerLimit = start.plusMonths(initMonth - 1)
          val upperLimit = start.plusMonths(endMonth - 1)

          value.isEqual(lowerLimit) ||
          value.isEqual(upperLimit) ||
          (value.isAfter(lowerLimit) && value.isBefore(upperLimit))
        })
      }

      case boundedPattern(operator, month) if month.toInt > 0 =>
        Success((start: LocalDateTime, value: LocalDateTime) => {
          val limit = start.plusMonths(month.toInt)

          operator match {
            case ">" => value.isAfter(limit) || value.isEqual(limit)
            case "<" => value.isBefore(limit) || value.isEqual(limit)
          }
        })

      case _ =>
        Failure(
          new IllegalArgumentException(
            "Interval does not match any of the patterns:  >M, <M, M1-M2 (M1 <= M2) "
          )
        )
    }

    comparatorFunction.map(new Interval(intervalString, _))
  }
}
