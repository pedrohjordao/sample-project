package sample

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import sample.models.Order
import sample.sources.{DataSource, RandomSource}

import scala.util.{Failure, Success, Try}

trait SampleProject {

  /**
    * Contains main logic of the application
    *
    * @param opts       the parsed command line parameters
    * @param dataSource a source of orders
    */
  def application(
      opts: Config,
      dataSource: DataSource[Order, Try]
  ): Try[String] = {
    val dateFilter: (LocalDateTime, LocalDateTime, LocalDateTime) => Boolean =
      (start, end, value) =>
        value.isEqual(start) || value.isEqual(end) || (value.isAfter(start) && value
          .isBefore(end))

    import PrettyPrinters._

    for {
      start <- DateParser.parse(opts.start)
      end <- DateParser.parse(opts.end)
      aggregator <- Aggregator.from(opts.intervals)
      originalData <- dataSource.getAll
      filtered <- Success(
        originalData.filter(order =>
          dateFilter(start, end, order.orderPlacementDate)
        )
      )
      aggr <- Success(aggregator(start, filtered))
      resultString <- aggr.prettyString()
    } yield {
      resultString
    }
  }
}

object SampleOrder extends SampleProject with App {
  Try { new CommandOpts(args.toSeq).intoConfig() }
    .map(opts => application(opts, new RandomSource(opts.seed)))
    .map {
      case Success(result) => println(result)
      case Failure(cause)  => System.err.println(cause.getMessage)
    }
}

object DateParser {
  val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

  /**
    * Parses a date in the format `yyyy-MM-dd HH:mm`
    *
    * @param string the date in a String format
    * @return parsed date if parsing was successful
    */
  def parse(string: String): Try[LocalDateTime] = Try {
    LocalDateTime.parse(string, formatter)
  }
}
