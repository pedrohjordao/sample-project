package sample.sources

/**
  * A generic representation of a data source
  *
  * This generic representation allows us to abstract over
  * the wrapper type being used
  *
  * @tparam A type used by the operations
  * @tparam F wrappper type for A, such as [[Try]] or [[Future]]
  */
trait DataSource[A, F[_]] {
  def getAll(): F[Seq[A]]
}
