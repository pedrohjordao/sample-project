package sample.sources

import java.time.{Instant, LocalDateTime, ZoneId, ZoneOffset}

import sample.models.{Customer, Order}

import scala.util.{Random, Try}

class RandomSource(seed: Long) extends DataSource[Order, Try] {
  private val lowerBound =
    LocalDateTime.of(2020, 1, 1, 0, 0).toInstant(ZoneOffset.UTC)
  private val upperBound =
    LocalDateTime.of(2022, 1, 1, 0, 0).toInstant(ZoneOffset.UTC)

  private val threadLocalRandom = new ThreadLocal[Random]
  threadLocalRandom.set(new Random(seed))

  /**
    * Generates 1000 data entries randomly for [[Order]]s
    *
    * These [[Order]]s do not contain any relevant data but the order's placement date.
    *
    * The randomly generated dates will always be between January 1st 2020 to January 1st 2022 UTC
    *
    * @return randomly generated orders
    */
  override def getAll(): Try[Seq[Order]] = Try {
    for {
      _ <- 0 to 1000
      randomEpochSeconds = rand
      asInstant = Instant.ofEpochSecond(randomEpochSeconds)
      orderDate = LocalDateTime.ofInstant(asInstant, ZoneId.of("UTC"))
    } yield {
      Order(
        Customer("Pedro Jordao", "+351 999 999 555"),
        "pedrojordao.pt",
        orderDate,
        Seq.empty
      )
    }
  }

  private def rand: Long =
    threadLocalRandom.get
      .nextLong(upperBound.getEpochSecond - lowerBound.getEpochSecond) + lowerBound.getEpochSecond
}
