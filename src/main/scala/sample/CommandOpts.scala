package sample

import org.rogach.scallop._

/**
  * Configuration type of the application
  *
  * @param start     start date
  * @param end       end date
  * @param seed      seed to generate random orders
  * @param intervals interval string
  */
case class Config(
    start: String,
    end: String,
    seed: Long,
    intervals: String
)

/**
  * Command line option parser
  *
  * @param arguments command line arguments
  */
class CommandOpts(private val arguments: Seq[String])
    extends ScallopConf(arguments) {
  version("0.1.0 2020 Pedro Jordão <pedrohjordao@gmail.com>")
  banner("""Counts orders by intervals
          |
          |Data is generated randomly with 1000 entries with dates
          |between 2020-01-01 00:00:00 and 2022-01-01 00:00:00
          |
          |There are 2 supported interval formats:
          |
          | * Simple Interval
          |   - Format: M1-M2 with M1 > 0 and M1 <= M2 
          |   - Will count the number of orders with dates 
          |     between (M1 - 1) and (M2 - 1) (inclusive) of
          |     the given --start date.
          |   - Example: 1-3 with a start date in January will 
          |     count orders with a month of January, Febuary
          |     and March.
          |
          | * Bounded Interval
          |   - Format: >M or <M with M > 0
          |   - Will count the number of orders in the interval
          |     up to M if <M or after M with M>, both being inclusive
          | """.stripMargin)

  val start =
    opt[String](
      required = true,
      descr = "start date to look for in the format yyyy-MM-dd HH:mm"
    )
  val end = opt[String](
    required = true,
    descr = "end date to look for in the format yyyy-MM-dd HH:mm"
  )

  val seed = opt[Long](
    required = true,
    descr = "seed to be used for generating random data, 42 by default",
    default = Some(42)
  )
  val intervals = opt[String](
    required = true,
    default = Some("1-3 4-6 7-12 >12"),
    descr = "intervals to aggregate, by default 1-3 4-6 7-12 >12"
  )

  verify()

  def intoConfig(): Config = Config(start(), end(), seed(), intervals())
}
