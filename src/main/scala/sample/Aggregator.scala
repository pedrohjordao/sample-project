package sample

import java.time.LocalDateTime

import sample.models.Order

import scala.util.{Failure, Success, Try}

/**
  * Aggregation through dates helper functions
  */
object Aggregator {

  /**
    * Typedef for functions used to aggregate orders through pre configured
    * intervals
    */
  type AggregatorFunction =
    (LocalDateTime, Seq[Order]) => Seq[(Interval, Int)]

  /**
    * Creates an [[AggregatorFunction]] from a configuration String
    *
    * The String must contain a space separated list of month intervals in the following format:
    *
    * {{{
    * M1-M2 with M1 <= M2
    * or
    * >M
    * or
    * <M
    * }}}
    *
    * ## Example:
    *
    * {{{
    * “1-3 4-6 7-12 >12”
    * }}}
    *
    * @param configuration a configuration String
    * @return an [[AggregationFunction]] if parsing was successful
    */
  def from(configuration: String): Try[AggregatorFunction] = {
    val intervals = configuration
      .split(" ")
      .map(Interval.from)
      .toSeq

    val (failures, successes) = intervals.partition(_.isFailure)

    if (failures.isEmpty) {
      Success(aggregate(successes.map(_.get)) _)
    } else {
      Failure(failures.head.failed.get)
    }
  }

  /**
    * Given a set of [[Interval]]s, produces an [[AggregatorFunction]]
    *
    * @param intervals      intervals to be used
    * @param startReference the reference used as the starting date
    * @param orders         orders to be evaluated
    * @return count of orders per interval
    */
  def aggregate(intervals: Seq[Interval])(
      startReference: LocalDateTime,
      orders: Seq[Order]
  ): Seq[(Interval, Int)] = {
    intervals.map(i =>
      (i, orders.count(o => i.contains(startReference, o.orderPlacementDate)))
    )
  }
}

/**
  * Implicit conversions for our data types
  */
object PrettyPrinters {

  implicit class AggregatorPrinter(
      private val aggregated: Seq[(Interval, Int)]
  ) extends AnyVal {

    def prettyString(): Try[String] = {
      val builder = new StringBuilder

      for {
        (interval, count) <- aggregated
      } yield {
        builder ++= s"${interval.asString} months: ${count} orders\n"
      }

      Success(builder.toString)
    }
  }
}
