package sample

import java.time.LocalDateTime

import sample.mocks.SuccessfulOrderSourceMock
import sample.models.Order
import sample.sources.DataSource
import org.scalatest.Inside
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.util.{Failure, Success, Try}

class sampleProjectSpec extends AnyFlatSpec with Inside {
  val app = new SampleProject {}
  val dataSource = new SuccessfulOrderSourceMock
  val defaultIntevalString = "1-3 4-6 7-12 >12"

  "SampleProject" should "generate string output containing all data if start and end date covers all data" in {
    val config = Config(
      "2020-01-01 00:00:00",
      "2030-01-01 00:00:00",
      0,
      defaultIntevalString
    )

    val expectedString = """1-3 months: 2 orders
                            |4-6 months: 1 orders
                            |7-12 months: 1 orders
                            |>12 months: 1 orders
                            |""".stripMargin

    inside(app.application(config, dataSource)) {
      case Success(result) => result shouldBe expectedString
    }
  }

  it should "filter out entries that are not between start-end" in {
    val config = Config(
      "2020-01-01 00:00:00",
      "2020-06-01 00:00:00",
      0,
      defaultIntevalString
    )

    val expectedString = """1-3 months: 2 orders
                           |4-6 months: 1 orders
                           |7-12 months: 0 orders
                           |>12 months: 0 orders
                           |""".stripMargin

    inside(app.application(config, dataSource)) {
      case Success(result) => result shouldBe expectedString
    }
  }

  it should "return empty intervals if no data falls within the intervals" in {
    val config = Config(
      "2022-01-01 00:00:00",
      "2030-01-01 00:00:00",
      0,
      defaultIntevalString
    )

    val expectedString = """1-3 months: 0 orders
                            |4-6 months: 0 orders
                            |7-12 months: 0 orders
                            |>12 months: 0 orders
                            |""".stripMargin

    inside(app.application(config, dataSource)) {
      case Success(result) => result shouldBe expectedString
    }
  }

  it should "parse bounded intervals correctly" in {
    val config =
      Config("2020-01-01 00:00:00", "2030-01-01 00:00:00", 0, "<11 >12")

    val expectedString = """<11 months: 4 orders
                           |>12 months: 1 orders
                           |""".stripMargin

    inside(app.application(config, dataSource)) {
      case Success(result) => result shouldBe expectedString
    }
  }

  it should "fail if data source fails" in {
    val config = Config(
      "2022-01-01 00:00:00",
      "2030-01-01 00:00:00",
      0,
      defaultIntevalString
    )
    val result = app.application(config, new DataSource[Order, Try] {
      override def getAll(): Try[Seq[Order]] =
        Failure(new IllegalArgumentException)
    })

    inside(result) {
      case Failure(_) => ()
    }
  }

  it should "fail if start date is not the correct format" in {
    val config = Config(
      "2022-01-0100:00:00",
      "2030-01-01 00:00:00",
      0,
      defaultIntevalString
    )
    val result = app.application(config, dataSource)

    inside(result) {
      case Failure(_) => ()
    }
  }

  it should "fail if end date is not the correct format" in {
    val config = Config(
      "2022-01-01 00:00:00",
      "2030-01-0100:00:00",
      0,
      defaultIntevalString
    )
    val result = app.application(config, dataSource)

    inside(result) {
      case Failure(_) => ()
    }

  }

  it should "fail if intervals is not the correct format" in {
    val config = Config("2022-01-01 00:00:00", "2030-01-01 00:00:00", 0, "")
    val result = app.application(config, dataSource)

    inside(result) {
      case Failure(_) => ()
    }

  }

  "DateParser" should "be successful parsing dates in the yyyy-MM-dd HH:mm:ss format" in {
    inside(DateParser.parse("1990-06-18 18:00:00")) {
      case Success(date) =>
        date shouldBe LocalDateTime.of(1990, 6, 18, 18, 0, 0)
    }
  }

  it should "fail to parse date in other format" in {
    inside(DateParser.parse("18-06-1990 18:00:00")) {
      case Failure(_) => ()
    }
  }
}
