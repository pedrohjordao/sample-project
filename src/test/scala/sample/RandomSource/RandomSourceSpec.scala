package sample.RandomSource

import sample.sources.RandomSource
import org.scalatest.Inside
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.util.Success

class RandomSourceSpec extends AnyFlatSpec with Inside {

  "Two RandomSources with the same seed" should "generate the exact same data" in {
    val source1 = new RandomSource(42)
    val source2 = new RandomSource(42)

    inside((source1.getAll, source2.getAll)) {
      case (Success(allFrom1), Success(allFrom2)) =>
        allFrom1 should contain theSameElementsInOrderAs allFrom2
    }
  }
}
