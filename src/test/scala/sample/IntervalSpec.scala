package sample

import java.time.LocalDateTime

import org.scalatest.Inside
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.util.{Failure, Success}

class IntervalTest extends AnyFlatSpec with Inside {
  "Interval.from" should "successfully create an instance of Interval for valid month intervals" in {
    val validIntervals = Seq(
      "1-3",
      "2-3",
      "1-1",
      "1-12",
      "12-13"
    )

    validIntervals.map(Interval.from).foreach {
      inside(_) {
        case Success(_) => ()
      }
    }
  }

  it should "successfully create an instance of Interval for valid month bounded intervals" in {
    val validIntervals = Seq(
      ">12",
      "<12",
      ">1",
      "<1",
      ">100"
    )

    validIntervals.map(Interval.from).foreach {
      inside(_) {
        case Success(_) => ()
      }
    }
  }

  it should "fail to create an instance of Interval for invalid month intervals" in {
    val invalidIntervals = Seq(
      "-1-2",
      "100",
      "0-1",
      "10-1",
      ""
    )

    invalidIntervals.map(Interval.from).foreach {
      inside(_) {
        case Failure(_) => ()
      }
    }
  }

  it should "fail to create an instance of Interval for invalid month bounded intervals" in {
    val invalidIntervals = Seq(
      ">0",
      "<0",
      "-10",
      "|5",
      " >10",
      "",
      " "
    )

    invalidIntervals.map(Interval.from).foreach {
      inside(_) {
        case Failure(_) => ()
      }
    }
  }

  "generated comparator" should "validate month intervals correctly" in {
    val maybeInterval = Interval.from("1-3")
    val startReference = LocalDateTime.of(2020, 1, 1, 0, 0)

    inside(maybeInterval) {
      case Success(interval) => {
        interval.contains(startReference, LocalDateTime.of(2020, 2, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2020, 1, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2020, 3, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2020, 4, 1, 0, 0)) shouldBe false
        interval.contains(startReference, LocalDateTime.of(2019, 12, 31, 0, 0)) shouldBe false
      }
    }
  }

  it should "validate month upper bounded intervals correctly" in {
    val maybeInterval = Interval.from("<12")
    val startReference = LocalDateTime.of(2020, 1, 1, 0, 0)

    inside(maybeInterval) {
      case Success(interval) => {
        interval.contains(startReference, LocalDateTime.of(2020, 2, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2020, 1, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2020, 3, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2020, 12, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2021, 1, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2019, 12, 31, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2021, 1, 2, 0, 0)) shouldBe false
        interval.contains(startReference, LocalDateTime.of(2021, 2, 1, 0, 0)) shouldBe false
      }
    }
  }

  it should "validate month lower bounded intervals correctly" in {
    val maybeInterval = Interval.from(">12")
    val startReference = LocalDateTime.of(2020, 1, 1, 0, 0)

    inside(maybeInterval) {
      case Success(interval) => {
        interval.contains(startReference, LocalDateTime.of(2020, 2, 1, 0, 0)) shouldBe false
        interval.contains(startReference, LocalDateTime.of(2020, 1, 1, 0, 0)) shouldBe false
        interval.contains(startReference, LocalDateTime.of(2020, 3, 1, 0, 0)) shouldBe false
        interval.contains(startReference, LocalDateTime.of(2020, 12, 1, 0, 0)) shouldBe false
        interval.contains(startReference, LocalDateTime.of(2019, 12, 31, 0, 0)) shouldBe false
        interval.contains(startReference, LocalDateTime.of(2021, 1, 1, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2021, 1, 2, 0, 0)) shouldBe true
        interval.contains(startReference, LocalDateTime.of(2021, 2, 1, 0, 0)) shouldBe true
      }
    }
  }
}
