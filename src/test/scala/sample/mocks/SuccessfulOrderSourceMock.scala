package sample.mocks

import java.time.LocalDateTime

import sample.models.Order
import sample.sources.DataSource

import scala.util.{Success, Try}

class SuccessfulOrderSourceMock extends DataSource[Order, Try] {
  override def getAll(): Try[Seq[Order]] = Success(
    Seq(
      Order(
        null,
        null,
        LocalDateTime.of(2019, 1, 1, 0, 0, 0),
        Seq.empty
      ), // before first interval
      Order(
        null,
        null,
        LocalDateTime
          .of(2020, 1, 1, 0, 0, 0),
        Seq.empty
      ), // at start date
      Order(
        null,
        null,
        LocalDateTime
          .of(2020, 2, 1, 0, 0, 0),
        Seq.empty
      ), // within first interval
      Order(
        null,
        null,
        LocalDateTime.of(2020, 5, 1, 0, 0, 0),
        Seq.empty
      ), // within second interval
      Order(
        null,
        null,
        LocalDateTime.of(2020, 11, 1, 0, 0, 0),
        Seq.empty
      ), // within third interval ,
      Order(
        null,
        null,
        LocalDateTime
          .of(2021, 1, 1, 0, 0, 0),
        Seq.empty
      ) //last bound
    )
  )
}
