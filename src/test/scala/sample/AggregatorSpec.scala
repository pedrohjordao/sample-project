package sample

import java.time.LocalDateTime

import sample.mocks.SuccessfulOrderSourceMock
import org.scalatest.Inside
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.util.{Failure, Success}

class AggregatorTest extends AnyFlatSpec with Inside {

  "Aggregator.from" should "succefully generate AgregatorFunction for valid configuration strings " in {
    val validConfigurations = Seq(
      "1-3 4-6 7-12 >12",
      ">12 <12",
      ">12",
      "1-3 1-5 >12 <12"
    )

    validConfigurations.map(Aggregator.from).foreach {
      inside(_) {
        case Success(_) => ()
      }
    }
  }

  it should "fail to generate AggregatorFunction for invalid configuration strings" in {
    val invalidConfigurations = Seq(
      "",
      "sample",
      "100"
    )

    invalidConfigurations.map(Aggregator.from).foreach {
      inside(_) {
        case Failure(_) => ()
      }
    }
  }

  "Aggregator.aggregate" should "correctly aggregate orders by interval" in {
    val intervals =
      "1-3 4-6 7-12 >12".split(" ").map(Interval.from).map(_.get)

    val orders = new SuccessfulOrderSourceMock().getAll.get

    val startDate = LocalDateTime.of(2020, 1, 1, 0, 0)

    val aggregatorF = Aggregator.aggregate(intervals.toSeq) _

    aggregatorF(startDate, orders) should contain theSameElementsAs Seq(
      (intervals(0), 2),
      (intervals(1), 1),
      (intervals(2), 1),
      (intervals(3), 1)
    )
  }

  "Pretty printer" should "generate string mapping interval to count" in {
    val aggregatorF = Aggregator.from("1-3 4-6 7-12 >12").get

    val startDate = LocalDateTime.of(2020, 1, 1, 0, 0)

    val orders = new SuccessfulOrderSourceMock().getAll.get

    import sample.PrettyPrinters._
    inside(aggregatorF(startDate, orders).prettyString) {
      case Success(pretty) =>
        pretty shouldBe """1-3 months: 2 orders
                          |4-6 months: 1 orders
                          |7-12 months: 1 orders
                          |>12 months: 1 orders
                          |""".stripMargin

    }
  }
}
