import Dependencies._

ThisBuild / scalaVersion := "2.13.2"
ThisBuild / version := "0.1.0"
ThisBuild / organization := "pt.pedrojordao"
ThisBuild / organizationName := "pedrojordao"
ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-Xlint",
  "-unchecked",
  "-Xfatal-warnings"
)

lazy val root = (project in file("."))
  .settings(
    name := "sample-project",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      scallop
    )
  )
