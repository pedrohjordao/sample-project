import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.1.2"
  lazy val scallop = "org.rogach" %% "scallop" % "3.4.0"
}
