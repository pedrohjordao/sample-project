# Sample Project

Filters randomly generated order information using a String format.

## Requirements

The project is built through [sbt](https://www.scala-sbt.org).

## Running the Application

To run the application execute `sbt run [options]` in the root folder of the project.

## Usage

For usage information run `sbt "run -h"`.

## Running Tests

Run unit tests with `sbt test`
